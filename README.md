## Setting up environment
```sh
conda env create -f environment.yml
```
## Usage
```sh
python -m greeter --count 2 --name Dima
```
